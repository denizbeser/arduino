#include <OneWire.h>

#define DS2413_ONEWIRE_PIN  (8)
#define ONEWIRE_CHECK_PIN_1 (6)
#define ONEWIRE_CHECK_PIN_2 (7)

#define DS2413_FAMILY_ID    0x3A
#define DS2413_ACCESS_READ  0xF5
#define DS2413_ACCESS_WRITE 0x5A
#define DS2413_ACK_SUCCESS  0xAA
#define DS2413_ACK_ERROR    0xFF

OneWire oneWire(DS2413_ONEWIRE_PIN);

void lookForDS2413(){
  Serial.println(F("Looking for DS2413s on the bus"));
  delay(700);
  byte i;
  byte present = 0;
  byte data[12];
  byte addr[8];

  //while there is a next address
  while(oneWire.search(addr)){

    //Print address
    Serial.print("R="); 
    for( i = 0; i < 8; i++) {
      Serial.print(addr[i], HEX);
      Serial.print(" ");
    }
  
    if ( OneWire::crc8( addr, 7) != addr[7]) {
        Serial.print("CRC is not valid!\n");
        return;
    }
    
    if ( addr[0] != DS2413_FAMILY_ID) {
        Serial.print("Device is not a DS2413 family device.\n");
        return;
    }
  
    // The DallasTemperature library can do all this work for you!
  
    oneWire.reset();
    oneWire.select(addr);
    oneWire.write(0x44,1);         // start conversion, with parasite power on at the end
    
    //delay(700);     // maybe 750ms is enough, maybe not
    // we might do a oneWire.depower() here, but the reset will take care of it.
    
    present = oneWire.reset();
    oneWire.select(addr);    
    oneWire.write(0xBE);         // Read Scratchpad
  
    Serial.print("P=");
    Serial.print(present,HEX);
    Serial.print(" ");
    for ( i = 0; i < 9; i++) {           // we need 9 bytes
      data[i] = oneWire.read();
      Serial.print(data[i], HEX);
      Serial.print(" ");
    }
    Serial.print(" CRC=");
    Serial.print( OneWire::crc8( data, 8), HEX);
    Serial.println();
    
    delay(700);
  }
  Serial.print("No more addresses.\n");
  oneWire.reset_search();
  delay(1000);
}

void setup(void) 
{
  pinMode(ONEWIRE_CHECK_PIN_1, OUTPUT);
  digitalWrite(ONEWIRE_CHECK_PIN_1, HIGH);
  
  pinMode(ONEWIRE_CHECK_PIN_2, OUTPUT);
  digitalWrite(ONEWIRE_CHECK_PIN_2, HIGH);
  
  Serial.begin(9600);    
}

void loop(void) 
{ 
  Serial.println("Transistor switches both open");
  digitalWrite(ONEWIRE_CHECK_PIN_1, LOW);
  digitalWrite(ONEWIRE_CHECK_PIN_2, LOW);
  lookForDS2413();
  
  Serial.println("Transistor switch 1 open, 2 closed");
  digitalWrite(ONEWIRE_CHECK_PIN_1, LOW);
  digitalWrite(ONEWIRE_CHECK_PIN_2, HIGH);
  lookForDS2413();
  
  Serial.println("Transistor switch 1 closed, 2 open");
  digitalWrite(ONEWIRE_CHECK_PIN_1, HIGH);
  digitalWrite(ONEWIRE_CHECK_PIN_2, LOW);
  lookForDS2413();
  
  Serial.println("Transistor switches both closed");
  digitalWrite(ONEWIRE_CHECK_PIN_1, HIGH);
  digitalWrite(ONEWIRE_CHECK_PIN_2, HIGH);
  lookForDS2413();
  Serial.println();
  delay(5000);
}
