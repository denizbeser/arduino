#include <NewPing.h>
#include <NewTone.h>

const uint8_t TRIGGER_PIN = 6;

const uint8_t ECHO_PIN = 7;

const uint8_t MAX_DISTANCE = 200;

const uint8_t ALARM = 3;

NewPing sonar(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE);

const uint8_t LED1 = 9, LED2 = 10;

bool state = false;

void setup() {

 Serial.begin(9600);

 pinMode(LED1, OUTPUT);

 pinMode(LED2, OUTPUT);
 
 pinMode(ALARM, OUTPUT);

 NewTone(ALARM, 440, 3000);

}
void loop() {

uint32_t uS = sonar.ping();

 Serial.print("Ping: ");

 int distance = (uS / US_ROUNDTRIP_CM);
 
 Serial.print(distance);

 int frequency = 80*distance;

 Serial.print("cm, ");
 
 Serial.print(frequency);
 
 Serial.println(" Hz");

 if(distance > 10){
  digitalWrite(LED1, LOW);

   digitalWrite(LED2, LOW);
 NewTone(ALARM, (frequency), 3000);
 }else{
  noNewTone(ALARM);
   digitalWrite(LED1, HIGH);

   digitalWrite(LED2, HIGH);
 }
 delay(50);

}

