  int switchPin_1 = 2;
  int switchPin_2 = 3;
  int switchPin_3 = 4;
  
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);  
  
  pinMode(switchPin_1, INPUT);
  pinMode(switchPin_2, INPUT);
  pinMode(switchPin_3, INPUT);

}

void loop() {
  
  // put your main code here, to run repeatedly:
  
  int switchState_1 = digitalRead(switchPin_1);
  int switchState_2 = digitalRead(switchPin_2);
  int switchState_3 = digitalRead(switchPin_3);
  
  Serial.println("New Run:");
  Serial.print(switchState_1);
  Serial.print(", ");
  Serial.print(switchState_2);
  Serial.print(", ");
  Serial.println(switchState_3);

  if(switchState_1 == 0){
    Serial.println("No main input");
  }
  else if(switchState_2 == LOW && switchState_3 == LOW){
    Serial.println("Orientation A");
  }
  else if(switchState_2 == LOW && switchState_3 == HIGH){
    Serial.println("Orientation B");
  }
  else if(switchState_2 == HIGH && switchState_3 == LOW){
    Serial.println("Orientation C");
  }
  else if(switchState_2 == HIGH && switchState_3 == HIGH){
    Serial.println("Orientation D");
  }
  delay(1000);
}
