#include <OneWire.h>
 
OneWire ow(2);
 
void setup() {
 Serial.begin(9600); 
}
 
void loop() {
 byte addr[8];
 ow.reset();
 if (!ow.search(addr)) {
  ow.reset_search();
  delay(2);
  return;
 }
 Serial.print("Found: ");
 for(byte i = 0; i < 8; i++) {
  Serial.print(addr[i], HEX);
  Serial.print(":");
 }
 Serial.println("");
 delay(1000);
}
