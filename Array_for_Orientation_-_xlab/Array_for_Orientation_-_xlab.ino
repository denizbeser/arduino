//declaration of switch pins 
  int switchPin_1 = 2;
  int switchPin_2 = 3;
  int switchPin_3 = 4;
  int switchPin_4 = 5;
  
//declaration of initial switch state values

  int switchState_1 = 0;
  int switchState_2 = 0;
  int switchState_3 = 0;
  int switchState_4 = 0;

  int orientationCount = 0;
  int stateChangeCount;

  int debounceCounter = 0;
  
  const int numberOfSwitches = 4;
  
  int switchArray[numberOfSwitches] ={switchPin_1, switchPin_2, switchPin_3, switchPin_4};

  int switchStateArray[numberOfSwitches];

  int finalOrientationNumber;
  
void setup() {
  
  // put your setup code here, to run once:
  
  Serial.begin(9600);  
  
  pinMode(switchPin_1, INPUT);
  pinMode(switchPin_2, INPUT);
  pinMode(switchPin_3, INPUT);
  pinMode(switchPin_4, INPUT);
}

void loop() {
  
  // put your main code here, to run repeatedly:

  stateArrayMaker();

  getOrientation(switchStateArray);
  
  delay(120);
}

// one function to set the array by reading the switches,

int stateArrayMaker(){
  
  Serial.println("New Run");
  
  for(int i = 0; i < numberOfSwitches; i++){
    
    int stateReading = digitalRead(switchArray[i]);
    
    //assign states to the array    
    switchStateArray[i] = stateReading;

    //print states
    if(i < (numberOfSwitches-1)){
    
      Serial.print(switchStateArray[i]);
      Serial.print(", "); 
    
    //new line if it is the last element of the array
    }else{
      Serial.println(switchStateArray[i]);
    }
  }
}

// one function to read the array and print orientation

int getOrientation(int array[]){

  //binary to integer
  
  for(int i = 0; i < numberOfSwitches; i++){

      stateChangeCount =  switchStateArray[i]*( (int) (pow(2,i) + 0.5) );
      
      orientationCount = orientationCount + stateChangeCount;   
  }
  
  finalOrientationNumber = orientationCount;
  
  Serial.print("Orientation ");  
  Serial.println(finalOrientationNumber);
  
  //set back to 0
  orientationCount = 0;
}

