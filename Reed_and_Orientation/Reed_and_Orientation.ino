#include <OneWire.h>
#include <Time.h>
#include "Delay.h"

//Sets pin 9 as onewire connection

OneWire ds(9);
    
  byte addr[8];
  byte result = 0x0;
  
//Debounce variables

  int switchState;
  int lastReedState[] = {0,0}; // This part is dependent on number of switches
  
  long lastDebounceTime = 0;
  long debounceDelay = 50;

//Nonblockingg delay declaration

  NonBlockDelay d;

//Orientation variables

  int orientationCount = 0;
  int stateChangeCount;

  int debounceCounter = 0;
  
  const int numberOfSwitches = 2;  //This part is also dependent on number of switches

  int stateArray[numberOfSwitches];

  int finalOrientation;
  int lastOrientation;
  
void setup() {
  ds.search(addr);
  // 
  Serial.begin(9600);  
  
}


void loop() {

  stateArrayMaker();

  getOrientation(stateArray);

  //Prints orientation if the orientations has changed
  
  if(lastOrientation != finalOrientation){
    Serial.println("New combination:");
    printStates();
    printOrientation();
    lastOrientation = finalOrientation;
  }
}

// This function forms the state array by reading the switches

int stateArrayMaker(){

  for(int i = 0; i < numberOfSwitches; i++){
    
    int stateReading = getState((i+1));

    //Debounce check

    if(stateReading != lastReedState[i]){
      lastDebounceTime = millis ();
    }
    
    //If more than debounceDelay time has passed
    
    if((millis() - lastDebounceTime) > debounceDelay){

      if(stateReading != switchState){        
        switchState = stateReading;

      }
      
      //Sets the state to the array
          
      stateArray[i] = switchState;
    }
    
    lastReedState[i] = stateReading;

  }
}

//This function gets the state from reed switch:

int getState(int switchNumber){

  //get the binary data form onewire
  
  ds.reset();
  ds.select(addr);
  ds.write(0xF5, 1);
  result = ds.read();

  /*If it's the first switch,
  then check the binary data and return 1 if closed, 0 if open */
  
  if(switchNumber == 1){
    if((result & 4) == 4){
      return 1;
    }else{
      return 0;
    }
  }else{ 

  /*If it's not the first switch, it must be second switch,
  then check the binary data and return 1 if closed, 0 if open */
    
    if((result & 1) == 1){
      return 1;
    }else{
      return 0;
    }
  }
}

// This function returns the orientation as an unique integer value

int getOrientation(int array[]){

  //Convert binary state array to integer
  
  for(int i = 0; i < numberOfSwitches; i++){

      stateChangeCount =  stateArray[i]*( (int) (pow(2,i) + 0.5) );
      
      orientationCount = orientationCount + stateChangeCount;   
  }
  
  finalOrientation = orientationCount;

  //Set back count to 0
  
  orientationCount = 0;

  return finalOrientation;
}

//This function prints states from the stateArray

int printStates(){
  for(int i = 0; i < numberOfSwitches; i++){
    if(i < (numberOfSwitches-1)){
          
      Serial.print(stateArray[i]);
      Serial.print(", "); 
    
    //new line if it is the last element of the array
    }else{
      Serial.println(stateArray[i]);
    }
  }
}
    
    

// This function prints orientation

int printOrientation(){
  Serial.print("Orientation ");  
  Serial.println(finalOrientation);
}

