#include "Delay.h"

//declaration of switch pins 
  const int switchPin_1 = 2;
  const int switchPin_2 = 3;
  const int switchPin_3 = 4;
  const int switchPin_4 = 5;

//debounce

  int switchState;
  int lastSwitchState[] = {0,0,0,0};
  
  long lastDebounceTime = 0;
  long debounceDelay = 50;

//Nonblockingg delay

  NonBlockDelay d;

//orientation

  int orientationCount = 0;
  int stateChangeCount;

  int debounceCounter = 0;
  
  const int numberOfSwitches = 4;
  
  int switchArray[numberOfSwitches] ={switchPin_1, switchPin_2, switchPin_3, switchPin_4};

  int switchStateArray[numberOfSwitches];

  int finalOrientation;
  int lastOrientation;
  
void setup() {
  
  // put your setup code here, to run once:
  
  Serial.begin(9600);  
  
  pinMode(switchPin_1, INPUT);
  pinMode(switchPin_2, INPUT);
  pinMode(switchPin_3, INPUT);
  pinMode(switchPin_4, INPUT);

  //nonblocking test
  pinMode(13, OUTPUT);
}

void loop() {

  if(d.Timeout()){
    
    if(digitalRead(13) == HIGH){
      digitalWrite(13, LOW);
    } else {
      digitalWrite(13, HIGH);
    }
    
    d.Delay(1000);
  }

  stateArrayMaker();

  getOrientation(switchStateArray);
  
  if(lastOrientation != finalOrientation){
    Serial.println("New combination:");
    printSwitchStates();
    printOrientation();
    lastOrientation = finalOrientation;
  }
}

// one function to set the array by reading the switches,

int stateArrayMaker(){
  
  for(int i = 0; i < numberOfSwitches; i++){
    
    int stateReading = digitalRead(switchArray[i]);

    if(stateReading != lastSwitchState[i]){
      lastDebounceTime = millis ();
    }
    //if more than debounceDelay time has passed
    if((millis() - lastDebounceTime) > debounceDelay){

      if(stateReading != switchState){        
        switchState = stateReading;

      }
      
      //assign states to the array    
      switchStateArray[i] = switchState;
    }

    lastSwitchState[i] = stateReading;

    
  }
}


// function to read the array

int getOrientation(int array[]){

  //binary to integer
  
  for(int i = 0; i < numberOfSwitches; i++){

      stateChangeCount =  switchStateArray[i]*( (int) (pow(2,i) + 0.5) );
      
      orientationCount = orientationCount + stateChangeCount;   
  }
  
  finalOrientation = orientationCount;

  //set back to 0
  orientationCount = 0;

  return finalOrientation;
}

//function to print states

int printSwitchStates(){
  for(int i = 0; i < numberOfSwitches; i++){
    if(i < (numberOfSwitches-1)){
          
      Serial.print(switchStateArray[i]);
      Serial.print(", "); 
    
    //new line if it is the last element of the array
    }else{
      Serial.println(switchStateArray[i]);
    }
  }
}
    
    

// function to print orientation

int printOrientation(){
  Serial.print("Orientation ");  
  Serial.println(finalOrientation);
}

